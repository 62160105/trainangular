import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  homefirstname:any="";
  homelastname:any="";
  homeage:any="";
  homelevel:any="";
  homecollege:any="";
  homelinkfacebook:any="";

  constructor() { }

  setname(firstname:any,lastname:any,age:any,level:any,college:any,linkfacebook:any){
    this.homefirstname=firstname ;
    this.homelastname= lastname ;
    this.homeage= age ;
    this.homelevel= level ;
    this.homecollege= college ;
    this.homelinkfacebook= linkfacebook ;

    console.log("test home page");
    console.log(this.homefirstname);
    console.log(this.homelinkfacebook);
  }
}
