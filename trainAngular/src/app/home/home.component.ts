import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InformationService } from '../information/information.service';
import { HomeService } from './home.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  information = new FormGroup({
    firstname:new FormControl(this.homeservice.homefirstname,Validators.required),
    lastname:new FormControl(this.homeservice.homelastname,Validators.required),
    age:new FormControl(this.homeservice.homeage,Validators.required),
    level:new FormControl(this.homeservice.homelevel,Validators.required),
    college:new FormControl(this.homeservice.homecollege,Validators.required),
    linkfacebook:new FormControl(this.homeservice.homelinkfacebook)
  });

  constructor(
    private informationservice:InformationService,
    private homeservice:HomeService,
    private router:Router
  ) { }

  ngOnInit(): void { 
  }
 
   

  gotonextpage(){ 
    
    if(this.information.valid == true){
      this.router.navigate(['information']);
    }
    else{
      Swal.fire("กรุณากรอกข้อมูลให้ครบถ้วน");
    }
 
    this.informationservice.setname(this.information.value);
     
  }
 

}
