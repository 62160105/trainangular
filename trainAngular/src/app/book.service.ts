import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  output:string = "";
  constructor() { }
  
getgrade(num:number){
  if(num>=80 && num<=100){
    this.output="A"
  }
  else if(num>=70 && num<80){
    this.output="B"
  }
  else if(num>=60 && num<70){
    this.output="C"
  }
  else if(num>=50 && num<60){
    this.output="D"
  }
  else if(num<50){
    this.output="F"
  }
  else {
    this.output="I DONT KNOW"
  }
  return this.output;
}

  
}
