import { Injectable } from '@angular/core'; 

@Injectable({
  providedIn: 'root'
})
export class InformationService { 
  infofirstname:any="";
  infolastname:any="";
  infoage:any="";
  infolevel:any="";
  infocollege:any="";
  infolinkfacebook:any="";

  constructor(
    

  ) { }
  setname(sendinformation:any){
    this.infofirstname=sendinformation['firstname'];
    this.infolastname=sendinformation['lastname'];
    this.infoage=sendinformation['age'];
    this.infolevel=sendinformation['level'];
    this.infocollege=sendinformation['college'];
    this.infolinkfacebook=sendinformation['linkfacebook'];

    // console.log("test information page");
    // console.log(this.infofirstname);
    // console.log(this.infolastname);
  }
   

}
