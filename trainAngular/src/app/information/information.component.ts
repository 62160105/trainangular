import {Component, Input, OnInit, Output ,EventEmitter } from '@angular/core';
import { Router } from '@angular/router'; 
import { InformationService } from './information.service';
import { HomeService } from '../home/home.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit { 
  firstnameinfo:string="";
  levelinfo:string="";
  linkfacebookinfo:string="";
  lastnameinfo:string="";
  agenameinfo:string="";
  collegenameinfo:string="";
  myimage:string = "assets/images/facebook.png";
  checkli = false;
  
  constructor(
    private router:Router,
    private informationservice:InformationService,
    private homeservice:HomeService
  ) { }

  ngOnInit(): void {
    this.firstnameinfo = this.informationservice.infofirstname;
    this.lastnameinfo = this.informationservice.infolastname;
    this.agenameinfo = this.informationservice.infoage;
    this.levelinfo = this.informationservice.infolevel;
    this.collegenameinfo = this.informationservice.infocollege;
    this.linkfacebookinfo = this.informationservice.infolinkfacebook;
    this.checklink();
    
    // console.log("test information page java");
    console.log(this.levelinfo);
  }

  gobackpage(){ 
    this.router.navigate(['']);
    this.homeservice.setname(this.firstnameinfo,this.lastnameinfo,this.agenameinfo,this.levelinfo,this.collegenameinfo,this.linkfacebookinfo);
  }

  checklink(){ 
    if(this.linkfacebookinfo!=""){ 
      this.checkli=true; 
    }
    // else{
    //   return this.link;
    // }
  }

  
}
